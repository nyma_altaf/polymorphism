#include<stdio.h>
#include<stdlib.h>

typedef (*vtable)(struct Matr *m);

vtable *matrix_table;
vtable *vector_table;
typedef struct Matrix {

int column;
int row;
int *array;
void (*Matrix)( struct Matr *mat, int col, int row);
void (*add)( struct Matr *mat, struct Mat *mat1, struct Mat *mat2);
void (*mult)( struct Matr *mat, struct Mat *mat1, struct Mat *mat2);
vtable *matrix_tb;
}Matr;

typedef struct Vector{
Matr matrix;
int max;
int min;
void (*vec)( struct vector *mat, int col, int row);
vtable *vector_tb;
}vect;

int L1_norm(Matr *m1){
int i=0;
int sum=0;
int j=0;
int final=0;
for(j=0; j<m1->column; j++){
for(i=0; i<(m1->row); i++){
sum=sum+m1->array[j*m1->column +i];
}
if(sum>final){
final=sum;
}
}

return final;

}

int L1_norm_v(Matr *m1){
int i=0;
int sum=0;
int j=0;
int final=0;
for(i=0; i<(m1->row); i++){
sum=sum+m1->array[i];

}

return sum;

}
void Add(Matr *m1, Matr *m2 , Matr *r)
{
int *arr;
  int i,j;

r->array=(int *)malloc( (m1->row*m1->column)* sizeof(int));

for(i=0;i<(m1->column*m1->row); i++){
	r->array[i]=m1->array[i]+m2->array[i];


}

}



void Multiply(Matr *m1, Matr *m2, Matr *r){
int f=0;
int i=0;
int j=0;
int k = 0;
int l=0;
int sum=0;

r->array=(int *)malloc( (m1->row*m2->column)* sizeof(int));
for ( i = 0; i<(m1->row*m1->column); i = i + m1->column){
		j = i;
		 f = 0;
		for (l = 0; l<(m2->row*m2->column);){
			sum = sum + (m1->array[j] * m2->array[l]);
 			l = l + m2->column;
			j++;
			if ((j) % m1->column == 0){
				r->array[k] = sum;
				j = i;
				if (l != ((m2->row*m2->column)-1) + m2->column){
				f++;
				l = f;
			}
			k++;
sum=0;
			}
		}
	
}
}
void Matrix_init( Matr *mat, int col, int row){
int **arr;
mat->add=Add;
mat->mult=Multiply;
mat->column=col;
mat->row= row;
mat->array=(int *)malloc(sizeof(int)*(row*col));
int i,j;


for(i=0;i<(col*row); i++){
mat->array[i] = i+1;
}

}
void V_Add(vect *m1, vect *m2 , vect *r)
{
int *arr;
  int i,j;
r->matrix.array=(int *)malloc( (m1->matrix.row*m1->matrix.column)* sizeof(int));

for(i=0;i<(m1->matrix.column*m1->matrix.row); i++){
	r->matrix.array[i]=m1->matrix.array[i]+m2->matrix.array[i];


}
}
void Vector_init( vect *mat, int col, int row){
int **arr;
mat->matrix.add=V_Add;
mat->matrix.mult=Multiply;
mat->matrix.column=col;
mat->matrix.row= row;
mat->matrix.array=(int *)malloc(sizeof(int)*(row*col));
int i,j;


for(i=0;i<(col*row); i++){
mat->matrix.array[i] = i+1;

}




}

int main(){

int matrix_row = 2; 
int matrix_column = 2; 
int vector_row = 2; 
int vector_column = 1;
Matr m1;
Matr m2;
Matr r;
Matr r1;
vect v1;
vect v2;
vect v3;
int i,j;
int in;	
printf("Press 1 for Vector Calculations\n"); 
printf("Press 2 for Matrix Calculations\n");
scanf("%d", &in);

if(in==1){
printf("Enter first vector rows:\n");
scanf("%d", &vector_row);
printf("Enter second vector rows:\n");
scanf("%d", &vector_column);
vector_table=(vtable *)malloc( (10)* sizeof(vtable));
v2.vec=Vector_init;
v2.vec(&v2, 1, vector_row);
v1.vec=Vector_init;
v1.vec(&v1, 1, vector_column);
if(vector_row==vector_column){
v1.matrix.add(&v1, &v2, &v3);
printf("Addition result of vector:\n");
for(i=0; i<v2.matrix.row;i++){

printf("%d  ", v3.matrix.array[i]);

printf("\n");
}
}
else
printf("Addition not possible!\n");

vector_table[0]=L1_norm_v;
v1.vector_tb=vector_table;
v2.vector_tb=vector_table;
printf("%d is L1 norm of first vector \n",v1.vector_tb[0](&v1.matrix));
printf("%d is L1 norm of second vector \n",v2.vector_tb[0](&v2.matrix));

}
else if(in==2){


printf("Enter first matrix rows:\n");
scanf("%d", &matrix_row);
printf("Enter first matrix columns:\n");
scanf("%d", &matrix_column);
printf("Enter second matrix rows:\n");
scanf("%d", &vector_row);
printf("Enter second matrix columns:\n");
scanf("%d", &vector_column);

m1.Matrix=Matrix_init;
m1.Matrix(&m1, matrix_row,matrix_column);
m2.Matrix=Matrix_init;
m2.Matrix(&m2, vector_row,vector_column);
	if (matrix_column == vector_column && matrix_row==vector_row){

m2.add(&m1, &m2,&r);
printf("Addition Result:\n");
for(i=0; i<m2.row;i++){
for (j=0; j<m2.column; j++){
printf("%d  ", r.array[i*m2.column +j]);
}
printf("\n");
}
}
else
printf("Addition not possible!\n");
//Matrix_init(ptr1, 2,2);
if(matrix_column==vector_row){
m2.mult(&m1,&m2, &r1);


printf("Multiplication Result:\n");
for(i=0; i<matrix_row;i++){
for (j=0; j<vector_column; j++){
printf("%d  ", r1.array[i*m2.column +j]);
}
printf("\n");
}
}
else
printf("Multiplication not possible!\n");
matrix_table=(vtable *)malloc( (10)* sizeof(vtable));
matrix_table[0]=L1_norm;
m1.matrix_tb=matrix_table;
m2.matrix_tb=matrix_table;
printf("%d is L1 norm of first matrix \n",m1.matrix_tb[0](&m1));
printf("%d is L1 norm of second matrix \n",m2.matrix_tb[0](&m2));

}

}
Nyma Altaf
Nyma Altaf
Nyma Altaf
Nyma Altaf
